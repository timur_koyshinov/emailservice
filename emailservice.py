#!/usr/bin/python3
# -*- coding: utf-8 -*-

import smtp
import pop3
import ereg

import socket
import config
from threading import Thread


def listen(host, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # sock.settimeout(config.autologout)
    sock.bind((host, port))
    sock.listen(100000)

    try:
        while True:
            conn, addr = sock.accept()
    
            if port == config.smtpport:
                thr = Thread(target=smtp.handler, args=(conn,))
            elif port == config.pop3port:
                thr = Thread(target=pop3.handler, args=(conn,))
            elif port == config.eregport:
                thr = Thread(target=ereg.handler, args=(conn,))
            else:
                return "Unknown problem: %d" % port
            thr.start()
    except Exception as e:
        print("Information about exception: ", e)
        conn.close()


thr_smtp = Thread(target=listen, args=(config.serverhost,
                                       config.smtpport))
thr_pop3 = Thread(target=listen, args=(config.serverhost,
                                       config.pop3port))
thr_ereg = Thread(target=listen, args=(config.serverhost,
                                       config.eregport))
thr_smtp.start()
thr_pop3.start()
thr_ereg.start()

print("Service started")


