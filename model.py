import peewee
import localconfig as secret


db = peewee.MySQLDatabase(secret.db,
    user   = secret.user,
    passwd = secret.passwd)


class BaseModel(peewee.Model):
    class Meta:
        database = db


class User(BaseModel):
    login  = peewee.CharField(primary_key=True)
    passwd = peewee.CharField()
    created_date = peewee.DateTimeField(default=peewee.datetime.datetime.now)

    def __str__(self):
        return self.login


class Letter(BaseModel):
    sender    = peewee.CharField()
    data      = peewee.TextField()
    size      = peewee.IntegerField()


class Owners(BaseModel):
    letter = peewee.ForeignKeyField(Letter, related_name='letter')
    owner  = peewee.ForeignKeyField(User, related_name='owner')
    fordelete = peewee.BooleanField()
