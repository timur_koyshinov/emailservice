import re
from model import *
import config
import base64


now_status = 'wait helo'
sender     = None
recipients = []


def handler(conn):
    global now_status

    conn.send(('220 smtp server is ready!\r\n').encode("utf-8"))

    try:
        while True:
            data = conn.recv(256).decode('utf-8')
            cmnd = re.match(r'\s*(\w{4})', data)
            if cmnd:
                cmnd = cmnd.group(0).lower()
                if cmnd in command:
                    if cmnd in status[now_status]:
                        if cmnd == 'helo' or cmnd == 'ehlo':
                            domain = re.match(r'\s*\w{4}\s+.+\s*', data)
                            if domain:
                                conn.send(helo(domain).encode("utf-8"))
                            else:
                                conn.send(('501 Syntax error in parameters or arguments!\r\n').encode("utf-8"))
                        elif cmnd == 'mail':
                            data = data.lower()
                            email = re.findall(r'\s*mail\s+from:\s*<?(\w{4,64}@[\w.]{4,32})>?', data)
                            if email:
                                conn.send(mail(email[0]).encode("utf-8"))
                            else:
                                conn.send(('501 Syntax error in parameters or arguments!\r\n').encode("utf-8"))
                        elif cmnd == 'rcpt':
                            data = data.lower()
                            email = re.findall(r'\s*rcpt\s+to:\s*<?(\w{4,64}@[\w.]{4,32})>?', data)
                            if email:
                                conn.send(rcpt(email[0]).encode("utf-8"))
                            else:
                                conn.send(('501 Syntax error in parameters or arguments!\r\n').encode("utf-8"))
                        elif cmnd == 'quit':
                            conn.close()
                            return
                        elif cmnd == 'data':
                            conn.send(datat(conn).encode("utf-8"))
                        else:
                            conn.send(command[cmnd]().encode("utf-8"))
                    else:
                        conn.send((errors[now_status]).encode("utf-8"))
                else:
                    conn.send(('502 Command not found or not implemented!\r\n').encode("utf-8"))
            else:
                conn.send(('500 Syntax error, command unrecognized!\r\n').encode("utf-8"))
    except UnicodeDecodeError:
        conn.close()
        return
    except Exception as e:
        return


def helo(domain):
    global now_status

    clear()

    if now_status == 'wait helo':
        now_status = 'wait mail'
    return "250 hello!\r\n"


def mail(reverse_path):
    global sender
    global now_status

    login = reverse_path.split('@')[0]

    if User.select().where(User.login == login):
        sender = reverse_path
        now_status = 'wait rcpt'
        return "250 okay\r\n"
    else:
        return "550 Requested action not taken: mailbox unavailable!\r\n"


def rcpt(forward_path):
    global recipients
    global now_status

    login = forward_path.split('@')[0]

    if User.select().where(User.login == login):
        if len(recipients)<10:
            if login in recipients:
                return "455 this recipient added earlier"
            recipients.append(login)
            now_status = 'wait data'
            return "250 ok\r\n"
        else:
            return "455 no more recipients!\r\n"
    else:
        return "550 Requested action not taken: mailbox unavailable!\r\n"


def datat(conn):
    global now_status
    global sender
    global recipients


    conn.send(('354 Start mail input; end with <CRLF>.<CRLF>\r\n').encode("utf-8"))

    try:
        dat = b''
        while dat[-5:] != b'\r\n.\r\n':
            dat += conn.recv(config.mailmaxsize)
            if len(dat[:-5]) > config.mailmaxsize:
                return "552 your letter too big\r\n"
        if len(dat[:-5]) > config.mailmaxsize:
            return "552 your letter too big\r\n"
        message = Letter.create(sender    = sender,
                                data      = base64.b64encode(dat[:-5]),
                                size      = len(dat[:-5]))

        for i in recipients:
            Owners.create(letter    = message,
                          owner     = i,
                          fordelete = False)
    except Exception as e:
        return "554 some problem\r\n"

    now_status = 'wait mail'
    sender     = None
    recipients = []

    return "250 ok\r\n"

        
def rset():
    global now_status
    global sender
    global recipients

    now_status = 'wait mail'
    sender     = None
    recipients = []

    return "250 reseted\r\n"


def noop():
    return "250 weee!\r\n"


def help():
    return """250 https://tools.ietf.org/html/rfc5321#section-4.1\r\n"""


command = {
    'ehlo'       : helo,
    'helo'       : helo,
    'mail'       : mail,
    'rcpt'       : rcpt,
    'data'       : datat,
    'rset'       : rset,
    'noop'       : noop,
    'help'       : help,
    'quit'       : None
}


status = {
    'wait helo' : ['helo', 'ehlo', 'quit', 'rset', 'noop', 'help'],
    'wait mail' : ['helo', 'ehlo', 'mail', 'quit', 'rset', 'noop', 'help'],
    'wait rcpt' : ['helo', 'ehlo', 'mail', 'rcpt', 'quit', 'rset', 'noop', 'help'],
    'wait data' : ['helo', 'ehlo', 'mail', 'rcpt', 'data', 'quit', 'rset', 'noop', 'help']
}


errors = {
    'wait helo' : '503 HELO first\r\n',
    'wait mail' : '503 MAIL first\r\n',
    'wait rcpt' : '503 MAIL/RCPT first\r\n'
}


def clear():
    global now_status
    global sender
    global recipients

    now_status = 'wait helo'
    sender     = None
    recipients = []