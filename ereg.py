import re
from model import *
from hashlib import sha256


def handler(conn):
    conn.send(('+OK ereg server is ready!\n').encode("utf-8"))
    try:
        while True:
            data = conn.recv(256).decode('utf-8')
            cmnd = re.match(r'\s*(\w{4,6})', data)
            if cmnd:
                cmnd = cmnd.group(0).lower()
                if cmnd in command:
                    if cmnd == 'create' or cmnd == 'delete':
                        if re.match(r'\s*\w{4,6}\s+\w{4,64}\s+\w{2,64}\s*', data):
                            login, passw = re.findall(
                                r'\s*\w{4,6}\s+(\w{4,64})\s+(\w{2,64})\s*',
                                data)[0]
                            conn.send(command[cmnd](login, passw).encode("utf-8"))
                        else:
                            conn.send(('-ERR probllem with arguments!\n').encode("utf-8"))
                    elif cmnd == 'list':
                        conn.send(command[cmnd]().encode("utf-8"))
                    elif cmnd == 'help':
                        conn.send(help().encode("utf-8"))
                    elif cmnd == 'quit':
                        conn.close()
                        return
                    else:
                        conn.send(('-ERR unknown error!\n').encode("utf-8"))
                else:
                    conn.send(('-ERR unknown command! Type: "HELP"\n').encode("utf-8"))
            else:
                conn.send(('-ERR syntax error!\n').encode("utf-8"))
    except UnicodeDecodeError:
        conn.close()
        return
    except Exception as e:
        conn.close()
        return


def create(login, passw):
    if User.select().where(User.login == login):
        return "-ERR this login exist!\n"
    else:
        User.create(
            login = login, 
            passwd = sha256(passw.encode('utf-8')).hexdigest())
    return "+OK mailbox created\n"


def delete(login, passw):
    passwd = User.select().where(User.login == login)
    if passwd:
        if passwd[0].passwd == sha256(passw.encode('utf-8')).hexdigest():
            dq = Owners.delete().where(Owners.owner == login)
            dq.execute()
            dq = User.delete().where(User.login == login)
            dq.execute()
        else:
            return "-ERR password is not correct!\n"
    else:
        return "-ERR this login doesn't exist!\n"
    return "+OK mailbox deleted\n"


# this command can not be deleted
# if you will do this, checker will humble pied
def listt():
    answ = '+OK Our 100 last users\n'
    for i in User.select().order_by(User.created_date.desc())[:100]:
        answ += i.login + "\n"
    return answ


def help():
    return """+OK
CREATE <login> <password> - create mailbox
DELETE <login> <password> - delete mailbox
LIST                      - last 100 users
QUIT                      - close connection
HELP                      - display this help message\n"""


command = {
    'create' : create,
    'delete' : delete,
    'list'   : listt,
    'quit'   : None,
    'help'   : help
}