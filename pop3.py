import re
from model import *
from hashlib import sha256
import base64


auth = False
login = None


def check_auth(func):
    def wrapper(*args, **kwargs):
        global auth
        if not auth:
            return "-ERR for this command autorizathion is required\r\n"
        else:
            return func(*args, **kwargs)
    return wrapper


def handler(conn):
    global password
    global login
    global auth

    conn.send(('+OK pop3 server ready!\r\n').encode("utf-8"))

    try:
        while True:
            data = conn.recv(256).decode('utf-8')
            cmnd = re.match(r'\s*(\w{4})', data)
            if cmnd:
                cmnd = cmnd.group(0).lower()
                if cmnd in command:
                    if cmnd == 'user':
                        if re.match(r'\s*\w{4}\s+\w{4,64}', data):
                            login = re.findall(
                                r'\s*\w{4}\s+(\w{4,64})', data)[0]
                            conn.send(user().encode("utf-8"))
                        else:
                            conn.send(('-ERR probllem with arguments!\r\n').encode("utf-8"))
                    elif cmnd == 'pass':
                        if re.match(r'\s*\w{4}\s+\w{2,64}', data):
                            password = re.findall(
                                r'\s*\w{4}\s+(\w{2,64})', data)[0]
                            conn.send(passw(password).encode("utf-8"))
                        else:
                            conn.send(('-ERR probllem with arguments!\r\n').encode("utf-8"))
                    elif cmnd == 'list':
                        if re.match(r'\s*\w{4}\s+\d+', data):
                            uid = re.findall(
                                r'\s*\w{4}\s+(\d+)', data)[0]
                            conn.send(listt(uid).encode("utf-8"))
                        else:
                            conn.send(listt(None).encode("utf-8"))
                    elif cmnd == 'dele' or cmnd == 'retr':
                        if re.match(r'\s*\w{4,5}\s+\d+', data):
                            uid = re.findall(
                                r'\s*\w{4,5}\s+(\d+)', data)[0]
                            conn.send(command[cmnd](uid).encode("utf-8"))
                        else:
                            conn.send(('-ERR probllem with arguments!\r\n').encode("utf-8"))
                    elif cmnd == 'quit':
                        q = Owners.delete().where(Owners.owner == login, Owners.fordelete == True)
                        q.execute()
                        login = None
                        password = None
                        auth = False
                        conn.send("+OK\r\n".encode("utf-8"))
                        conn.close()
                        return               
                    else:
                        conn.send(command[cmnd]().encode("utf-8"))
                else:
                    conn.send(('-ERR unknown command! Type: "HELP"\r\n').encode("utf-8"))
            else:
                conn.send(('-ERR syntax error!\r\n').encode("utf-8"))

    except UnicodeDecodeError:
        conn.close()
        return
    except Exception as e:
        return


def user():
    if User.select().where(User.login == login):
        return "+OK %s is a valid mailbox\r\n" % login
    else:
        return "-ERR sorry, this login does not exist!\r\n"


def passw(password):
    global auth
    pas = sha256(password.encode('utf-8')).hexdigest()
    if not login:
        return "-ERR using USER please\r\n"
    if User.select().where(User.login == login and User.passwd == pas):
        auth = True
        return "+OK mailbox locked and ready\r\n"
    else:
        return "-ERR invalid password\r\n"


@check_auth
def stat():
    soz = 0
    countt = Owners.select().where(Owners.owner_id == login, 
                                   Owners.fordelete == False).count()
    for i in Owners.select().where(Owners.owner_id == login, 
                                   Owners.fordelete == False):
        let = Letter.select().where(Letter.id == i.letter).get()
        soz = let.size + soz
    return "+OK %d %d\r\n" % (countt, soz)


@check_auth
def listt(uid):
    if uid == None:
        answ = stat()[:-2].split(' ')
        answ = "%s %s messages (%s octets):\r\n" % tuple(answ)
        for i in Owners.select().where(Owners.owner == login, 
                                                 Owners.fordelete == False):
            let = Letter.select().where(Letter.id == i.letter).get()
            answ += "%s %s\r\n" % (let.id, let.size)
        answ += ".\r\n"
        return answ
    else:
        ow = Owners.select().where(Owners.owner == login, 
                                   Owners.fordelete == False,
                                   Owners.letter == uid)
        if ow:
            ow = ow.get()
            lett = Letter.select().where(Letter.id == ow.letter_id).get()
            return "+OK %s %s\r\n" % (uid, lett.size)
        else:
            return "-ERR no such message\r\n"
    

@check_auth
def retr(uid):
    let= Owners.select().where(Owners.owner == login, Owners.letter == uid)
    if let:
        letter = Letter.select().where(Letter.id == uid)
        data = '+OK %s octets\r\n' % letter.get().size
        data += base64.b64decode(letter.get().data.encode('utf-8')).decode('utf-8')
        return data + "\r\n.\r\n"
    else:
        return "-ERR no such message\r\n" 


@check_auth
def dele(uid):
    lett = Owners.select().where(Owners.owner == login, Owners.letter == uid)
    if lett:
        if lett.get().fordelete == False:
            q = Owners.update(fordelete=True).where(Owners.owner == login, 
                                                    Owners.letter == uid)
            q.execute()
            return "+OK message %s deleted\r\n" % uid       
        else:
            return "-ERR message %s already deleted\r\n" % uid
    else:
        return "-ERR no such message\r\n"


@check_auth
def noop():
    return "+OK\r\n"


@check_auth
def rset():
    q = Owners.update(fordelete=False).where(Owners.owner == login)
    q.execute()

    return "+OK basket is empty\r\n"


def help():
    return """+OK https://tools.ietf.org/html/rfc1081\r\n"""


command = {
    'user'  : user,
    'pass'  : passw,
    'stat'  : stat,
    'list'  : listt,
    'retr'  : retr,
    'dele'  : dele,
    'noop'  : noop,
    'rset'  : rset,
    'quit'  : None,
    'help'  : help    
}


